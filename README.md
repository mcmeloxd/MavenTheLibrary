# TheLibrary

#### 介绍
servlet+jsp实现图书管理<br />
只是在之前图书馆系统的基础上加入了servlet和jsp<br />
直接用之前图书馆系统的方法和数据库即可。



#### 使用说明
:wink: 注册，登录，添加图书，查询借阅信息已做好，其他有兴趣可以试着做一下。<br />
:innocent: 前端部分功能使用了jsp自带的语法标签，可以参考https://www.runoob.com/jsp/jsp-syntax.html。
