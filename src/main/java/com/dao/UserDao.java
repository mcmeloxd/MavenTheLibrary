package com.dao;

import com.entry.Book;
import com.entry.BorrowInfo;
import com.entry.User;

import java.util.List;

/**
 * --- Be Humble and Hungry ---
 *
 * @author McMeloxD
 * @date 2024/6/26
 * @desc
 */
public interface UserDao {
    User login(String username, String password);
    boolean register(User user);
    List<BorrowInfo> borrowInfo(User user);
    List<Book> addBook(Book book);

}
