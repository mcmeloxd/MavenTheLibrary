package com.servlet;

import com.entry.User;
import com.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * --- Be Humble and Hungry ---
 *
 * @author McMeloxD
 * @date 2024/6/26
 * @desc 注册
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 0 设置编码格式
        // req.setCharacterEncoding("utf-8");
        // resp.setContentType("text/html;charset=utf-8");

        // 1接收请求数据
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        // 2将数据传递给业务层
        boolean flag = userService.register(new User(username,password));

        // 3做出响应
        PrintWriter writer = resp.getWriter( );
        if (flag) {// 说明登录成功
            // writer.write("<html>");
            // writer.write("<head>");
            // writer.write("<title>登录成功</title>");
            // writer.write("</head>");
            // writer.write("<body>");
            // writer.write("<h1>歪日他姐，[ "+ username +" ]您已注册成功,请登录~ </h1>");
            // writer.write("</body>");
            // writer.write("</html>");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        } else {
            // writer.write("<html>");
            // writer.write("<head>");
            // writer.write("<title>注册失败</title>");
            // writer.write("</head>");
            // writer.write("<body>");
            // writer.write("<h1 style='color:red'>该账号已经注册过！</h1>");
            // writer.write("</body>");
            // writer.write("</html>");
            req.setAttribute("msg","抱歉，该账号已存在！请重试~");
            req.getRequestDispatcher("register.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
