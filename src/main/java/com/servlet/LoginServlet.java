package com.servlet;

import com.entry.User;
import com.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * --- Be Humble and Hungry ---
 *
 * @author McMeloxD
 * @date 2024/6/26
 * @desc
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private UserServiceImpl loginService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 0 设置编码格式
        // req.setCharacterEncoding("utf-8");
        // resp.setContentType("text/html;charset=utf-8");

        // 1接收请求数据
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        // 2将数据传递给业务层
        User user = loginService.login(username, password);
        // 3做出响应
        // PrintWriter writer = resp.getWriter( );
        if (user != null) {// 说明登录成功
            // writer.write("<html>");
            // writer.write("<head>");
            // writer.write("<title>登录成功</title>");
            // writer.write("</head>");
            // writer.write("<body>");
            // writer.write("<h1>歪日他姐，欢迎 [ "+user.getUname()+" ]</h1>");
            // writer.write("<hr>");
            // writer.write("<h3>个人信息如下:</h3>");
            // writer.write("<table border='2'>");
            // writer.write("<thead>");
            // writer.write("<td>编号</td>");
            // writer.write("<td>用户名</td>");
            // writer.write("<td>密码</td>");
            // writer.write("</thead>");
            // writer.write("<tr>");
            // writer.write("<td>"+user.getUid()+"</td>");
            // writer.write("<td>"+user.getUname()+"</td>");
            // writer.write("<td>"+user.getPassword()+"</td>");
            // writer.write("</tr>");
            // writer.write("</table>");
            // writer.write("</body>");
            // writer.write("</html>");
            req.setAttribute("msg","登陆成功，欢迎"+ username + "回家~");
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } else {
            // writer.write("<html>");
            // writer.write("<head>");
            // writer.write("<title>登录失败</title>");
            // writer.write("</head>");
            // writer.write("<body>");
            // writer.write("<h1 style='color:red'>用户名或密码错误</h1>");
            // writer.write("</body>");
            // writer.write("</html>");
            req.setAttribute("msg","用户名或密码错误，请重试~");
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
