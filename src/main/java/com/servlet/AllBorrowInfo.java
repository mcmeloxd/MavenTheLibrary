package com.servlet;

import com.entry.BorrowInfo;
import com.entry.User;
import com.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * --- Be Humble and Hungry ---
 *
 * @author McMeloxD
 * @date 2024/6/26
 * @desc
 */
@WebServlet("/allInfo")
public class AllBorrowInfo extends HttpServlet {
    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 0 设置编码格式
        // req.setCharacterEncoding("utf-8");
        // resp.setContentType("text/html;charset=utf-8");

        // 1接收请求数据
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        // 2将数据传递给业务层
        List<BorrowInfo> infos = userService.borrowInfo(user);
        if (infos.size() > 0) {
            //如果返回有数据，交给页面
            req.setAttribute("infos", infos);
            req.getRequestDispatcher("allInfo2.jsp").forward(req, resp);
        }else {
            //没有数据，弹出提醒,在页面控制跳转
            req.setAttribute("msg", "您目前没有来借过图书哦~");
        }

        // 3做出响应

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
