package com.service;

import com.entry.Book;
import com.entry.BorrowInfo;
import com.entry.User;

import java.util.ArrayList;
import java.util.List;

/**
 * --- Be Humble and Hungry ---
 *
 * @author Liyuexian
 * @date 2024/6/26
 * @desc
 */
public interface UserService {
    User login(String username, String password);
    boolean register(User user);
    List<BorrowInfo> borrowInfo(User user);
    List<Book> addBook(Book book);
}
