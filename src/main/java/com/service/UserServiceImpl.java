package com.service;

import cn.hutool.Hutool;
import cn.hutool.crypto.SecureUtil;
import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.entry.Book;
import com.entry.BorrowInfo;
import com.entry.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * --- Be Humble and Hungry ---
 *
 * @author McMeloxD
 * @date 2024/6/26
 * @desc
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public User login(String username, String password) {
        //加密
        User user = userDao.login(username,SecureUtil.md5(password));
        return user;
    }

    @Override
    public boolean register(User user) {
        //加密
        user.setPassword(SecureUtil.md5(user.getPassword()));
        boolean flag = userDao.register(user);
        return flag;
    }

    @Override
    public List<BorrowInfo> borrowInfo(User user) {
        List<BorrowInfo> infos = userDao.borrowInfo(user);
        return infos;
    }

    @Override
    public List<Book> addBook(Book book) {
        List<Book> books = userDao.addBook(book);
        return books;
    }
}
