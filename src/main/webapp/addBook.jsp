
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>添加图书</title>
    <style>
        body {
            background: url("static/bg3.png") no-repeat;
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            text-align: center;
            margin: 0;
            padding: 20px;
        }

        table {
            width: 80%;
            margin: 20px auto;
            border-collapse: collapse;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        table, th, td {
            border: 1px solid #ccc;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<form action="/addBook" method="post">
    <h2>图书添加</h2>
    <table>
        <thead>
        <tr>
            <th>书名</th>
            <th>价格</th>
            <th>出版社</th>
            <th>作者</th>
            <th>可借阅数量</th>
        </tr>
        </thead>
        <tbody id="bookList">
        <tr>
            <td><input type="text" name="bname"></td>
            <td><input type="number" name="price" step="0.01"></td>
            <td><input type="text" name="press"></td>
            <td><input type="text" name="author"></td>
            <td><input type="number" name="number"></td>
        </tr>
        </tbody>
    </table>
    <button type="submit">添加</button>
    <button type="button" onclick="btn()">返回</button>
</form>

<script>
    function btn() {
        window.location = "index.jsp";
    }
</script>
<%
    String info = (String) request.getAttribute("msg");
    if (info != null) {
%>
<script>
    //提醒
    alert("<%= info  %>");
    // 跳转
    window.location = "index.jsp";
</script>
<%
    }
%>
</body>
</html>