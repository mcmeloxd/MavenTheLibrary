<%@ page import="java.util.List" %>
<%@ page import="com.entry.Book" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>图书借阅记录*</title>
    <style>
        body {
            background: url("static/bg3.png") no-repeat;
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            text-align: center;
            margin: 0;
            padding: 20px;
        }

        table {
            width: 80%;
            margin: 20px auto;
            border-collapse: collapse;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        table, th, td {
            border: 1px solid #ccc;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <h2>图书信息</h2>
    <table>
        <thead>
        <tr>
            <th>图书ID</th>
            <th>书名</th>
            <th>作者</th>
            <th>价格</th>
            <th>出版社</th>
            <th>可借阅数量</th>
        </tr>
        </thead>
        <tbody id="bookList">
        <!-- 这里用JavaScript动态生成表格内容 -->
        </tbody>
    </table>
    <button type="button" onclick="btn()">返回</button>
</form>
    <script>
    var booksData = [
        <%
            List<Book> books = (List<Book>) request.getAttribute("books");
            for (int i = 0; i < books.size(); i++) {
                if (i == books.size() - 1) {
                    out.println(books.get(i));
                }else out.println(books.get(i)+",");
            }
        %>
    // 可以根据实际情况添加更多数据
    ];
    // 获取表格的tbody元素
    var tbody = document.getElementById("bookList");

    // 循环遍历数据，动态生成表格内容
    booksData.forEach(function(book) {
        console.log(book)
        var row = document.createElement("tr");
        row.innerHTML = `
                <td>${book.bid}</td>
                <td>${book.bname}</td>
                <td>${book.price}</td>
                <td>${book.press}</td>
                <td>${book.author}</td>
                <td>${book.number}</td>
            `;
        tbody.appendChild(row);
    });


    function btn() {
        window.location = "index.jsp";
    }
</script>
</body>
</html>