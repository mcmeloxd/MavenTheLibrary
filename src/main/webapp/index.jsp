<%@ page import="com.entry.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>
    body {
        margin: 0;
        padding: 0;
        background: url("static/bj3.png") no-repeat;
    }

    #app {
        text-align: center;
        border: 1px solid rgb(208, 197, 192);
        background-color: rgb(237, 229, 226);
        width: 500px;
        height: 420px;
        margin: 100px auto;
        padding-top: 15px;
        font-family: 幼圆;
        line-height: 10px;
        font-weight: bold;
    }

    a {
        color: rgb(80, 145, 61);
    }
</style>
<body>

<div id="app">
    <h3>图书借阅系统</h3>
    <img src="static/wenjie.png" style="border-radius: 50%;width: 80px;height: 80px">
    <p>当前登录用户:
        <%
            User user = (User) session.getAttribute("user");
            out.print(user.getUname());
        %>
    </p>
    <br>
    <p><a href="addBook.jsp">*** 1.添加图书 ***</a></p>
    <p><a href="">*** 2.删除图书 ***</a></p>
    <p><a href="">*** 3.搜索图书 ***</a></p>
    <p><a href="allInfo.jsp">*** 4.产看已借阅图书信息 ***</a></p>
    <p><a href="">*** 5.查看未归还图书信息 ***</a></p>
    <p><a href="">*** 6.借书 ***</a></p>
    <p><a href="">*** 7.还书 ***</a></p>
    <p><a href="/logout">*** 8.退出登录 ***</a></p>
</div>
</body>
</html>
