<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录页面</title>
    <style>
        body {
            background: url("static/bg2.png") no-repeat;
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            text-align: center;
            margin: 0;
            padding: 0;
        }
        .login-container {
            margin-top: 100px;
            width: 300px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            padding: 20px;
            display: inline-block;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .login-container h2 {
            margin-bottom: 20px;
        }
        .form-group {
            margin-bottom: 20px;
        }
        .form-group label {
            display: block;
            text-align: left;
            margin-bottom: 5px;
        }
        .form-group input {
            width: calc(100% - 20px);
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        .form-group input:focus {
            outline: none;
            border-color: #66afe9;
        }
        .form-group button {
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            border: none;
            color: white;
            cursor: pointer;
            border-radius: 3px;
        }
        .form-group button:hover {
            background-color: #45a049;
        }
        .register-link {
            margin-top: 10px;
        }
        .register-link a {
            color: #1e90ff;
            text-decoration: none;
        }
        .register-link a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<div class="login-container">
    <h2>用户登录</h2>
    <form id="loginForm" action="/login" method="post" onsubmit="return validateForm()">
        <div class="form-group">
            <label for="username">用户名:</label>
            <input type="text" id="username" name="username" required>
        </div>
        <div class="form-group">
            <label for="password">密码:</label>
            <input type="password" id="password" name="password" required>
        </div>
        <div class="form-group">
            <button type="submit">登录</button>
        </div>
    </form>
    <div class="register-link">
        <p>还没有账号？<a href="register.jsp">点击这里注册</a></p>
    </div>
</div>

<script>
    function validateForm() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;

        // 在这里可以添加更多的验证逻辑，如长度、特殊字符等

        // 简单示例，只验证用户名和密码非空
        if (username.trim() === "" || password.trim() === "") {
            alert("用户名和密码不能为空");
            return false;
        }

        // 后续可以通过Ajax提交到服务器进行验证

        return true; // 返回true则提交表单，返回false则不提交
    }
</script>
<%
    String info = (String)request.getAttribute("msg");
    if (info!=null){
%>
<script>
    alert("<%= info  %>");
</script>
<%
    }
%>
</body>
</html>