<%@ page import="java.util.List" %>
<%@ page import="com.entry.BorrowInfo" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>图书借阅记录</title>
    <style>
        body {
            background: url("static/bg3.png") no-repeat;
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            text-align: center;
            margin: 0;
            padding: 20px;
        }

        table {
            width: 80%;
            margin: 20px auto;
            border-collapse: collapse;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        table, th, td {
            border: 1px solid #ccc;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<form action="/allInfo" method="post">
    <h2>图书借阅记录</h2>
    <table>
        <thead>
        <tr>
            <th>图书ID</th>
            <th>书名</th>
            <th>作者</th>
            <th>价格</th>
            <th>借阅时间</th>
            <th>是否归还</th>
        </tr>
        </thead>
        <tbody id="bookList">
        <!-- 这里用JavaScript动态生成表格内容 -->
        </tbody>
    </table>
    <button type="submit">查询</button>
    <button type="button" onclick="btn()">返回</button>
</form>

<script>
    function btn() {
        window.location = "index.jsp";
    }
</script>
<%
    String info = (String) request.getAttribute("msg");
    if (info != null) {
%>
<script>
    //提醒
    alert("<%= info  %>");
    // 跳转
    window.location = "index.jsp";
</script>
<%
    }
%>
</body>
</html>